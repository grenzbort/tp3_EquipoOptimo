package negocio;
import java.util.ArrayList;

public class Jugador
{
		public Integer calidad;
		public String posicion;
		public String posicionEspecifica;
		public String nombreYapellido;
		public ArrayList<String> incompatibles;
		
		public Jugador(int cal, String pos, String posEspecifica, String nom)
		{
			nombreYapellido = nom;
			calidad = cal;
			posicion = pos;
			posicionEspecifica = posEspecifica;
			incompatibles = new ArrayList<String>();
		}

		public void agregarIncompatible(Jugador j)
		{
			this.incompatibles.add(j.nombreYapellido);
			j.incompatibles.add(this.nombreYapellido);
		}
		
		public void borrarIncompatible(Jugador j)
		{
			this.incompatibles.remove(j.nombreYapellido);
			j.incompatibles.remove(this.nombreYapellido);
		}

		public boolean tieneConflictosCon(Jugador j)
		{
			return incompatibles.contains(j.nombreYapellido);
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + ((calidad == null) ? 0 : calidad.hashCode());
			result = prime * result + ((incompatibles == null) ? 0 : incompatibles.hashCode());
			result = prime * result + ((nombreYapellido == null) ? 0 : nombreYapellido.hashCode());
			result = prime * result + ((posicion == null) ? 0 : posicion.hashCode());
			result = prime * result + ((posicionEspecifica == null) ? 0 : posicionEspecifica.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Jugador other = (Jugador) obj;
			if (calidad == null) {
				if (other.calidad != null)
					return false;
			} else if (!calidad.equals(other.calidad))
				return false;
			if (incompatibles == null) {
				if (other.incompatibles != null)
					return false;
			} else if (!incompatibles.equals(other.incompatibles))
				return false;
			if (nombreYapellido == null) {
				if (other.nombreYapellido != null)
					return false;
			} else if (!nombreYapellido.equals(other.nombreYapellido))
				return false;
			if (posicion == null) {
				if (other.posicion != null)
					return false;
			} else if (!posicion.equals(other.posicion))
				return false;
			if (posicionEspecifica == null) {
				if (other.posicionEspecifica != null)
					return false;
			} else if (!posicionEspecifica.equals(other.posicionEspecifica))
				return false;
			return true;
		}
}