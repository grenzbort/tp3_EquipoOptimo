package negocio;

import interfazGrafica.Notificacion;

public class GeneradorEquipo
{
	static Equipo generarDesdeArquero(String formacion, boolean conConflictos)
	{
		Equipo ideal = new Equipo(formacion.charAt(0), formacion.charAt(2), formacion.charAt(4));

		if(!AgregarJugadores.agregarArquero(ideal, conConflictos))
		{
			mostrar("ARQ");
		}
		else if(!AgregarJugadores.agregarDefensores(formacion.charAt(0), ideal, conConflictos))
		{
			mostrar("DEF");
		}
		else if(!AgregarJugadores.agregarVolantes(formacion.charAt(2), ideal, conConflictos))
		{
			mostrar("VOL");
		}
		else if(!AgregarJugadores.agregarDelanteros(formacion.charAt(4), ideal, conConflictos))
		{
			mostrar("DEL");
		}
		if(ideal.equipoCompleto())
		{
			return ideal;
		}
		return null;
	}
	
	static Equipo generarDesdeDefensor(String formacion, boolean conConflictos)
	{
		Equipo ideal = new Equipo(formacion.charAt(0), formacion.charAt(2), formacion.charAt(4));
		
		if(!AgregarJugadores.agregarDefensores(formacion.charAt(0), ideal, conConflictos))
		{
			mostrar("DEF");
		}
		else if(!AgregarJugadores.agregarArquero(ideal, conConflictos))
		{
			mostrar("ARQ");
		}
		else if(!AgregarJugadores.agregarVolantes(formacion.charAt(2), ideal, conConflictos))
		{
			mostrar("VOL");
		}
		else if(!AgregarJugadores.agregarDelanteros(formacion.charAt(4), ideal, conConflictos))
		{
			mostrar("DEL");
		}
		if(ideal.equipoCompleto())
		{
			return ideal;
		}
		return null;
	}
	
	static Equipo generarDesdeVolante(String formacion, boolean conConflictos)
	{
		Equipo ideal = new Equipo(formacion.charAt(0), formacion.charAt(2), formacion.charAt(4));
		
		if(!AgregarJugadores.agregarVolantes(formacion.charAt(2), ideal, conConflictos))
		{
			mostrar("VOL");
		}
		else if(!AgregarJugadores.agregarDelanteros(formacion.charAt(4), ideal, conConflictos))
		{
			mostrar("DEL");
		}
		else if(!AgregarJugadores.agregarArquero(ideal, conConflictos))
		{
			mostrar("ARQ");
		}
		else if(!AgregarJugadores.agregarDefensores(formacion.charAt(0), ideal, conConflictos))
		{
			mostrar("DEF");
		}
		if(ideal.equipoCompleto())
		{
			return ideal;
		}
		return null;
	}

	static Equipo generarDesdeDelantero(String formacion, boolean conConflictos)
	{
		Equipo ideal = new Equipo(formacion.charAt(0), formacion.charAt(2), formacion.charAt(4));
			
		if(!AgregarJugadores.agregarDelanteros(formacion.charAt(4), ideal, conConflictos))
		{
			mostrar("DEL");
		}
		else if(!AgregarJugadores.agregarVolantes(formacion.charAt(2), ideal, conConflictos))
		{
			mostrar("VOL");
		}
		else if(!AgregarJugadores.agregarDefensores(formacion.charAt(0), ideal, conConflictos))
		{
			mostrar("DEF");
		}
		else if(!AgregarJugadores.agregarArquero(ideal, conConflictos))
		{
			mostrar("ARQ");
		}
		if(ideal.equipoCompleto())
		{
			return ideal;
		}
		return null;
	}
	
	private static void mostrar(String pos)
	{
		if (pos.equals("ARQ"))
		{
			Notificacion.notificar("Le falto un arquero para completar el equipo. Convoque mas jugadores!", "Inicio");
		}
		else if (pos.equals("DEF"))
		{
			Notificacion.notificar("Le faltaron defensores para completar el equipo. Convoque mas jugadores!", "Inicio");
		}
		else if (pos.equals("VOL"))
		{
			Notificacion.notificar("Le faltaron volantes para completar el equipo. Convoque mas jugadores!", "Inicio");
		}
		else
		{
			Notificacion.notificar("Le faltaron delanteros para completar el equipo. Convoque mas jugadores!", "Inicio");
		}
	}
}
