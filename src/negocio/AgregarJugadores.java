package negocio;

import java.util.ArrayList;

public class AgregarJugadores
{
	@SuppressWarnings("static-access")
	static boolean agregarArquero(Equipo ideal, boolean tieneConflictos)
	{
		ArrayList<Jugador> jugadores = PrepararEquipo.getArqueros().get("Arquero");

		for(Jugador j : jugadores)
		{
			if(ideal.agregarArquero(j, tieneConflictos))
			{
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("static-access")
	static boolean agregarDefensores(char cantDefensores, Equipo ideal, boolean tieneConflictos)
	{
		Integer cantDef = Integer.parseInt(String.valueOf(cantDefensores));
		ArrayList<Jugador> LI = PrepararEquipo.getDefensores().get("LI");
		ArrayList<Jugador> LD = PrepararEquipo.getDefensores().get("LD");
		ArrayList<Jugador> DFC = PrepararEquipo.getDefensores().get("DFC");

		boolean agregueLI = false;
		boolean agregueLD = false;
		boolean agregueDFC = false;
		boolean agregueDFC2 = false;
		boolean agregueDFC3 = false;

		for(Jugador j : LI)
		{
			if(ideal.agregarDefensor(j, tieneConflictos))
			{
				agregueLI = true;
				cantDef--;
				break;
			}
		}
		for(Jugador j : LD)
		{
			if(ideal.agregarDefensor(j, tieneConflictos))
			{
				agregueLD = true;
				cantDef--;
				break;
			}
		}
		if(cantDef < 3) 
		{
			agregueDFC3 = true;
		}
		if(cantDef < 2) 
		{
			agregueDFC2 = true;
		}

		for(Jugador j : DFC)
		{
			if(cantDef>0 && ideal.agregarDefensor(j, tieneConflictos))
			{
				if(cantDef == 3)
				{
					agregueDFC3 = true;
				}
				else if(cantDef == 2) 
				{
					agregueDFC2 = true;
				}
				else if(cantDef == 1) 
				{
					agregueDFC = true;
				}
				cantDef--;
			}
		}	
		return agregueLI && agregueLD && agregueDFC && agregueDFC2 && agregueDFC3;
	}

	@SuppressWarnings({ "static-access", "unused" })
	static boolean agregarVolantes(char cantVolantes, Equipo ideal, boolean tieneConflictos)
	{
		Integer cantVol = Integer.parseInt(String.valueOf(cantVolantes));
		ArrayList<Jugador> MI = PrepararEquipo.getVolantes().get("MI");
		ArrayList<Jugador> MD = PrepararEquipo.getVolantes().get("MD");
		ArrayList<Jugador> MC = PrepararEquipo.getVolantes().get("MC");
		
		boolean agregueMI = false;
		boolean agregueMD = false;
		boolean agregueMC = false;
		boolean agregueMC2 = false;
		boolean agregueMC3 = false;

		for(Jugador j : MI)
		{
			if(ideal.agregarVolante(j, tieneConflictos))
			{
				agregueMI = true;
				cantVol--;
				break;
			}
		}
		for(Jugador j : MD)
		{
			if(ideal.agregarVolante(j, tieneConflictos))
			{
				agregueMD=true;
				cantVol--;
				break;
			}
		}
		if(cantVol < 3)
		{
			agregueMC3 = true;
		}
		if(cantVol < 2) 
		{
			agregueMC2 = true;
		}
		
		for(Jugador j : MC)
		{
			if(cantVol > 0 && ideal.agregarVolante(j, tieneConflictos))
			{
				if(cantVol == 3)
				{
					agregueMC3 = true;
				}
				else if(cantVol==2)
				{
					agregueMC2 = true;
				}
				else if(cantVol == 1)
				{
					agregueMC = true;
				}
				cantVol--;
			}
		}
		return agregueMI && agregueMD && agregueMC;
	}

	@SuppressWarnings("static-access")
	static boolean agregarDelanteros(char cantDelanteros, Equipo ideal, boolean tieneConflictos)
	{
		Integer cantDel = Integer.parseInt(String.valueOf(cantDelanteros));
		ArrayList<Jugador> DC = PrepararEquipo.getDelanteros().get("DC");
		
		boolean agregueDC = false;
		boolean agregueDC2 = false;

		for(Jugador j: DC)
		{
			if(ideal.agregarDelantero(j, tieneConflictos))
			{
				agregueDC=true;
				cantDel--;
				if(cantDel==2 || cantDel==0)
				{
					agregueDC2 = true;
					break;	
				}
			}
		}
		if( agregueDC && cantDel == 2)
		{
			ArrayList<Jugador> EI = PrepararEquipo.getDelanteros().get("EI");
			ArrayList<Jugador> ED = PrepararEquipo.getDelanteros().get("ED");
			
			boolean agregueED = false;
			boolean agregueEI = false;

			for(Jugador j : ED)
			{
				if(ideal.agregarDelantero(j, tieneConflictos))
				{
					agregueED=true;
					cantDel--;
					break;
				}
			}
			for(Jugador j: EI)
			{
				if(ideal.agregarDelantero(j, tieneConflictos))
				{
					agregueEI=true;
					cantDel--;
					break;
				}
			}
			return agregueDC && agregueEI && agregueED;
		}
		return (agregueDC && agregueDC2 && cantDel == 0);
	}
}
