package negocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import interfazGrafica.VentanaFormacionOptima;

public class PrepararEquipo
{
	private static HashMap<String, ArrayList<Jugador>> arqueros = new HashMap<String, ArrayList<Jugador>>();
	private static HashMap<String, ArrayList<Jugador>> defensores = new HashMap<String, ArrayList<Jugador>>();
	private static HashMap<String, ArrayList<Jugador>> volantes = new HashMap<String, ArrayList<Jugador>>();
	private static HashMap<String, ArrayList<Jugador>> delanteros = new HashMap<String, ArrayList<Jugador>>();
	
	private static Equipo equipoIdeal;

	
	public PrepararEquipo(String formacion, String algoritmoAusar, boolean tieneConflictos)
	{
		inicializarIdeal(formacion, algoritmoAusar, tieneConflictos);
	}

	private void inicializarIdeal(String formacion, String algoritmoAusar, boolean tieneConflictos)
	{
		iniciarPosiciones();
		cargarTodosLosJugadores();
		ordenarPorCalidad();
		
		Equipo ideal = armarEquipo(algoritmoAusar, formacion, tieneConflictos);
		equipoIdeal = ideal;
		
		if(ideal != null)
		{
			VentanaFormacionOptima.resultado(ideal, formacion);
		}
	}
	
	private void iniciarPosiciones()
	{
		arqueros.put("Arquero", new ArrayList<Jugador>());
		
		defensores.put("LD", new ArrayList<Jugador>());
		defensores.put("LI", new ArrayList<Jugador>());
		defensores.put("DFC", new ArrayList<Jugador>());
		
		volantes.put("MI", new ArrayList<Jugador>());
		volantes.put("MD", new ArrayList<Jugador>());
		volantes.put("MC", new ArrayList<Jugador>());
		
		delanteros.put("EI", new ArrayList<Jugador>());
		delanteros.put("ED", new ArrayList<Jugador>());
		delanteros.put("DC", new ArrayList<Jugador>());
	}
	
	private void cargarTodosLosJugadores()
	{
		JugadorAJson.cargar(JugadorAJson.archivo);
		ArrayList<Jugador> todosLosJugadores = JugadorAJson.jugadores;
		ArrayList<Jugador> jugadoresEspecificos = new ArrayList<Jugador>();
		
		for(Jugador j : todosLosJugadores)
		{
			if(j.posicion.equals("Arquero"))
			{
				jugadoresEspecificos = arqueros.get(j.posicionEspecifica);
				jugadoresEspecificos.add(j);
				arqueros.put(j.posicionEspecifica, jugadoresEspecificos);
			}
			else if(j.posicion.equals("Defensor"))
			{
				jugadoresEspecificos = defensores.get(j.posicionEspecifica);
				jugadoresEspecificos.add(j);
				defensores.put(j.posicionEspecifica, jugadoresEspecificos);
			}
			else if(j.posicion.equals("Volante"))
			{
				jugadoresEspecificos = volantes.get(j.posicionEspecifica);
				jugadoresEspecificos.add(j);
				volantes.put(j.posicionEspecifica, jugadoresEspecificos);
			}
			else
			{
				jugadoresEspecificos = delanteros.get(j.posicionEspecifica);
				jugadoresEspecificos.add(j);
				delanteros.put(j.posicionEspecifica, jugadoresEspecificos);
			}
		}			
	}
	
	private void ordenarPorCalidad()
	{
		Collections.sort(arqueros.get("Arquero"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		
		Collections.sort(defensores.get("LD"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		Collections.sort(defensores.get("LI"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		Collections.sort(defensores.get("DFC"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		
		Collections.sort(volantes.get("MD"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		Collections.sort(volantes.get("MI"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		Collections.sort(volantes.get("MC"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		
		Collections.sort(delanteros.get("ED"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		Collections.sort(delanteros.get("EI"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
		Collections.sort(delanteros.get("DC"), (o1, o2) -> o2.calidad.compareTo(o1.calidad));
	}

	private Equipo armarEquipo(String algoritmoAusar, String formacion, boolean tieneConflictos)
	{
		if(algoritmoAusar.equals("ARQ"))
		{
			return GeneradorEquipo.generarDesdeArquero(formacion, tieneConflictos);
		}
		if(algoritmoAusar.equals("DEF"))
		{
			return GeneradorEquipo.generarDesdeDefensor(formacion, tieneConflictos);
		}
		if(algoritmoAusar.equals("VOL"))
		{
			return GeneradorEquipo.generarDesdeVolante(formacion, tieneConflictos);
		}	
		if(algoritmoAusar.equals("DEL"))
		{
			return GeneradorEquipo.generarDesdeDelantero(formacion, tieneConflictos);
		}		
		return null;
	}
	
	public static HashMap<String, ArrayList<Jugador>> getArqueros()
	{
		return arqueros;
	}

	public static HashMap<String, ArrayList<Jugador>> getDefensores()
	{
		return defensores;
	}

	public static HashMap<String, ArrayList<Jugador>> getVolantes()
	{
		return volantes;
	}

	public static HashMap<String, ArrayList<Jugador>> getDelanteros()
	{
		return delanteros;
	}

	public Equipo getEquipoIdeal()
	{
		return equipoIdeal;
	}
}
