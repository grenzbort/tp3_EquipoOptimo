package negocio;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import interfazGrafica.Notificacion;

public class Serializacion
{
	public static void cargarJugadores()
	{
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Json","file");
		fileChooser.setDialogTitle("Cargar");
		fileChooser.setFileFilter(filter);

		int result = fileChooser.showOpenDialog(null);

		if(result == JFileChooser.APPROVE_OPTION)
		{
			try
			{
				File selectedFile = fileChooser.getSelectedFile();
				String path = selectedFile.getAbsolutePath();
				JugadorAJson.setPath(path);
				JugadorAJson.cargar(path);  
			}
			catch(Exception e)
			{
				Notificacion.notificar("Por favor, seleccione un archivo Json", "Inicio");
			}
		}
		else if(result == JFileChooser.CANCEL_OPTION)
		{
		}
	}

	public static void guardarJugadores()
	{
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Json","file");
		fileChooser.setDialogTitle("Guardar");
		fileChooser.addChoosableFileFilter(filter);

		int result = fileChooser.showSaveDialog(null);

		if(result == JFileChooser.APPROVE_OPTION)
		{   	  
			File selectedFile = fileChooser.getSelectedFile();
			String path = selectedFile.getAbsolutePath()+".Json";
			JugadorAJson.setPath(path);
			JugadorAJson.actualizarArchivo(path);
			JugadorAJson.cargar(path); 
		}         
	}
}
