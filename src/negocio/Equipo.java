package negocio;

import java.util.ArrayList;

public class Equipo
{

	private static ArrayList<Jugador> equipo;
	private static Jugador[] arquero;
	private static Jugador[] defensores;
	private static Jugador[] volantes;
	private static Jugador[] delanteros;

	public Equipo(int cantDefensores, int cantVolantes, int cantDelanteros)
	{
		equipo = new ArrayList<Jugador>();
		arquero = new Jugador[1];
		defensores = new Jugador[cantDefensores];
		volantes = new Jugador[cantVolantes];
		delanteros = new Jugador[cantDelanteros];
	}

	public boolean equipoCompleto()
	{
		return equipo.size() == 11;
	}

	public static boolean agregarArquero(Jugador jugador, boolean tieneConflictos)
	{
		if(tieneConflictos)
		{
			for(Jugador jug : equipo)
			{
				if(jug != null && jug.tieneConflictosCon(jugador))
				{
					return false;
				}
			}
		}
		arquero[0] = jugador;
		equipo.add(jugador);
		return true;
	}

	public static boolean agregarDefensor(Jugador jugador, boolean tieneConflictos)
	{
		if(tieneConflictos)
		{
			for(Jugador jug: equipo)
			{
				if(jug != null &&  jug.tieneConflictosCon(jugador))
				{
					return false;
				}
			}
		}
		for(int i=0; i<defensores.length;i++)
		{
			if(defensores[i] == null)
			{
				defensores[i] = jugador;
				equipo.add(jugador);
				return true;
			}
		}
		return false;
	}

	public static boolean agregarVolante(Jugador jugador, boolean tieneConflictos)
	{
		if(tieneConflictos)
		{
			for(Jugador jug: equipo)
			{
				if(jug!=null &&  jug.tieneConflictosCon(jugador))
				{
					return false;
				}
			}
		}
		for(int i=0; i<volantes.length;i++)
		{
			if(volantes[i]==null)
			{
				volantes[i]=jugador;
				equipo.add(jugador);
				return true;
			}
		}
		return false;
	}

	public static boolean agregarDelantero(Jugador jugador, boolean tieneConflictos)
	{
		if(tieneConflictos)
		{
			for(Jugador jug: equipo)
			{
				if(jug!=null &&  jug.tieneConflictosCon(jugador))
				{
					return false;
				}
			}
		}
		for(int i=0; i<delanteros.length;i++)
		{
			if(delanteros[i]==null)
			{
				delanteros[i]=jugador;
				equipo.add(jugador);
				return true;
			}
		}
		return false;
	}

	public int cantDel()
	{
		int cant=0;
		for(Jugador j : equipo)
		{
			if(j.posicion.equals("Delantero"))
				cant++;
		}
		return cant;
	}

	public int cantDef()
	{
		int cant=0;
		for(Jugador j : equipo) 
		{
			if(j.posicion.equals("Defensor"))
				cant++;
		}
		return cant;
	}

	public int cantVol()
	{
		int cant = 0;
		for(Jugador j : equipo)
		{
			if(j.posicion.equals("Volante"))
				cant++;
		}
		return cant;
	}

	public int getCalidad()
	{
		int ret = 0;
		for(Jugador j : equipo)
		{
			ret += j.calidad;
		}
		return ret;
	}

	public static ArrayList<Jugador> getEquipo()
	{
		return equipo;
	}
}
