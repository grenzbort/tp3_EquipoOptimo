package negocio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import interfazGrafica.Notificacion;

public class JugadorAJson
{
	public static ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
	public static String archivo = "jugadores";

	public static void guardarJugador(Jugador jugador)
	{	
		cargar("jugadores");

		if(!jugadores.isEmpty())
		{
			if(contiene(jugador))
			{
				Notificacion.notificar("El jugador "+ jugador.nombreYapellido +" ya se encuentra en el equipo.", "ingresoJugador");
				return;
			}
		}
		jugadores.add(jugador);
		
		Collections.sort(jugadores, new Comparator<Jugador>()
		{
			@Override   
			public int compare(Jugador obj1, Jugador obj2)
			{
				return obj1.nombreYapellido.compareTo(obj2.nombreYapellido);
			}
		});
		actualizarArchivo(archivo);
		Notificacion.notificar("El jugador " + jugador.nombreYapellido +" fue a�adido al equipo.", "Inicio");
	}

	public static void borrarJugador(String jugador)
	{
		Jugador jugadorBorrar = obtenerJugador(jugador);
		jugadores.remove(jugadorBorrar);
		actualizarArchivo(archivo); 
		Notificacion.notificar("El jugador "+ jugadorBorrar.nombreYapellido +" fue eliminado del equipo.", "Inicio");				
	}

	public static void actualizarArchivo(String path)
	{
		Gson gson = new Gson();	
		String json = gson.toJson(jugadores);
		guardar(json,path);	
	}

	public static void guardar(String json,String nombre)
	{
		try
		{
			FileWriter writer = new FileWriter(nombre);
			writer.write(json);
			writer.close();
		} 
		catch(Exception e) 
		{
			System.out.println("ERROR");
		}
	}

	private static boolean contiene(Jugador jugador)
	{
		for(Jugador j : jugadores)
		{
			if(j.equals(jugador))
			{
				return true;
			}
		}
		return false;
	}

	public static Jugador obtenerJugador(String nombre)
	{
		for(Jugador j : jugadores)
		{
			if(j.nombreYapellido.equals(nombre))
				return j;
		}
		return null;
	}

	public static void actualizarJugador(Jugador j1, Jugador j2)
	{
		jugadores.set(jugadores.indexOf(j1), j1);
		jugadores.set(jugadores.indexOf(j2), j2);
		actualizarArchivo(archivo);
	}

	public static void cargar(String nombre)
	{
		Gson gson = new Gson();
		JsonParser jsonParser = new JsonParser();
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(nombre));
			JsonElement jsonElement = jsonParser.parse(br);
			Type type = new TypeToken<List<Jugador>>() {}.getType();
			jugadores = gson.fromJson(jsonElement, type);

		} 
		catch (IOException e) {}      
	}

	public static void setPath(String direccion)
	{
		archivo = direccion;
	}
}