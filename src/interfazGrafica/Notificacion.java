package interfazGrafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;

public class Notificacion {

	private JFrame frmMensaje;
	private static String origen;
	private static String estado;

	public static void notificar(String Estado, String Origen)
	{
		origen = Origen;
		estado = Estado;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Notificacion window = new Notificacion();
					window.frmMensaje.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Notificacion() {
		initialize();
	}

	private void initialize() {
		frmMensaje = new JFrame();
		frmMensaje.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBorrarJugador.class.getResource("/imagenes/Icono Ventana.png")));
		frmMensaje.setTitle("Notificación");
		frmMensaje.setResizable(false);
		frmMensaje.setBounds(100, 100, 357, 274);
		frmMensaje.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnNewButton = new JButton("ACEPTAR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frmMensaje.dispose();
				if(origen == "ingresoJugador")
					VentanaNuevoJugador.frmAgregarJugador.setVisible(true);
				else if(origen == "Inicio")
					MainForm.frmInicio.setVisible(true);
				else if(origen == "IngresoIncompatibilidad")
					VentanaNuevoConflicto.frmGenerarConflicto.setVisible(true);
				
			}
		});
		frmMensaje.getContentPane().setLayout(null);
		btnNewButton.setFont(new Font("Lucida Sans", Font.BOLD, 14));
		btnNewButton.setBounds(10, 182, 125, 52);
		frmMensaje.getContentPane().add(btnNewButton);	
		
		JTextPane txtEstado = new JTextPane();
		txtEstado.setFont(new Font("Lucida Sans", Font.BOLD, 20));
		txtEstado.setText(estado);
		txtEstado.setEditable(false);
		txtEstado.setBackground(Color.WHITE);
		txtEstado.setBounds(10, 14, 331, 108);
		frmMensaje.getContentPane().add(txtEstado);
		
		JLabel fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(Notificacion.class.getResource("/imagenes/Fondo Mensaje.png")));
		fondo.setBounds(0, 0, 351, 245);
		frmMensaje.getContentPane().add(fondo);
	}

}
