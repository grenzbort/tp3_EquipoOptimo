package interfazGrafica;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JLabel;

import negocio.JugadorAJson;
import negocio.Jugador;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class VentanaNuevoConflicto
{

	static JFrame frmGenerarConflicto;

	public static void ingresoIncompatibilidad()
	{
		EventQueue.invokeLater(new Runnable()
		{
			@SuppressWarnings("static-access")
			public void run()
			{
				try
				{
					VentanaNuevoConflicto window = new VentanaNuevoConflicto();
					window.frmGenerarConflicto.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaNuevoConflicto()
	{
		initialize();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize()
	{
		frmGenerarConflicto = new JFrame();
		frmGenerarConflicto.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBorrarJugador.class.getResource("/imagenes/Icono Ventana.png")));
		frmGenerarConflicto.setTitle("Generar Conflicto");
		frmGenerarConflicto.setResizable(false);
		frmGenerarConflicto.setBounds(100, 100, 520, 251);
		frmGenerarConflicto.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGenerarConflicto.getContentPane().setLayout(null);
		
		JComboBox boxJugador1 = new JComboBox();
		boxJugador1.setBounds(10, 36, 242, 22);
		boxJugador1.setFont(new Font("Lucida Sans", Font.BOLD, 12));
		boxJugador1.setMaximumRowCount(999);
		frmGenerarConflicto.getContentPane().add(boxJugador1);
		
		JComboBox boxJugador2 = new JComboBox();
		JugadorAJson.cargar(JugadorAJson.archivo);
		for(Jugador j : JugadorAJson.jugadores)
		{
			boxJugador1.addItem(j.nombreYapellido);
			boxJugador2.addItem(j.nombreYapellido);
		}
		boxJugador2.setBounds(10, 94, 242, 22);
		boxJugador2.setMaximumRowCount(999);
		boxJugador2.setFont(new Font("Lucida Sans", Font.BOLD, 12));
		frmGenerarConflicto.getContentPane().add(boxJugador2);
		
		JLabel lblJugador1 = new JLabel("JUGADOR 1");
		lblJugador1.setBounds(79, 11, 104, 22);
		lblJugador1.setFont(new Font("Lucida Sans", Font.BOLD, 15));
		frmGenerarConflicto.getContentPane().add(lblJugador1);
		
		JLabel lblJugador2 = new JLabel("JUGADOR 2");
		lblJugador2.setBounds(79, 69, 104, 22);
		lblJugador2.setFont(new Font("Lucida Sans", Font.BOLD, 15));
		frmGenerarConflicto.getContentPane().add(lblJugador2);		
		
		JButton btnAgregar = new JButton("AGREGAR");
		btnAgregar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				frmGenerarConflicto.setVisible(false);
				if(JugadorAJson.jugadores.isEmpty())
				{
					Notificacion.notificar("No hay ningun jugador ingresado", "IngresoIncompatibilidad");
				}
				else
				{
					Jugador j1 = JugadorAJson.obtenerJugador(boxJugador1.getSelectedItem().toString());
					Jugador j2 = JugadorAJson.obtenerJugador(boxJugador2.getSelectedItem().toString());
					if(!j1.incompatibles.contains(j2.nombreYapellido))
					{
						if(j1.equals(j2)){
							Notificacion.notificar(j1.nombreYapellido+" no puede tener conflictos con si mismo.", "IngresoIncompatibilidad");
						}
							
						else{
							j1.agregarIncompatible(j2);
							Notificacion.notificar(j1.nombreYapellido+" ahora es incompatible con "
												+ j2.nombreYapellido, "IngresoIncompatibilidad");
						}
					}
					else
						Notificacion.notificar(j1.nombreYapellido+" y "+j2.nombreYapellido+" ya tienen un conflicto.", "IngresoIncompatibilidad");
				JugadorAJson.actualizarJugador(j1,j2);
				}
			}
		});
		btnAgregar.setBounds(10, 158, 155, 46);
		btnAgregar.setFont(new Font("Lucida Sans", Font.BOLD, 15));
		frmGenerarConflicto.getContentPane().add(btnAgregar);
		
		JButton btnBorrar = new JButton("BORRAR");
		btnBorrar.setBounds(175, 158, 155, 46);
		btnBorrar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				frmGenerarConflicto.setVisible(false);
				if(JugadorAJson.jugadores.isEmpty()){
					Notificacion.notificar("No hay ningun jugador ingresado", "IngresoIncompatibilidad");
				}	
				else
				{
					Jugador j1 = JugadorAJson.obtenerJugador(boxJugador1.getSelectedItem().toString());
					Jugador j2 = JugadorAJson.obtenerJugador(boxJugador2.getSelectedItem().toString());
					if(!j1.incompatibles.contains(j2.nombreYapellido))
					{	
						Notificacion.notificar(j1.nombreYapellido+" y "+j2.nombreYapellido+" no tienen ningun conflicto.", "IngresoIncompatibilidad");
					}
					else
					{
						j1.borrarIncompatible(j2);
						Notificacion.notificar(j1.nombreYapellido+" y "+j2.nombreYapellido+" ya no tienen ningun conflicto.", "IngresoIncompatibilidad");
					}
					JugadorAJson.actualizarJugador(j1,j2);
				}
			}
		});
		btnBorrar.setFont(new Font("Lucida Sans", Font.BOLD, 15));
		frmGenerarConflicto.getContentPane().add(btnBorrar);
		
		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.setBounds(340, 158, 155, 46);
		btnCancelar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				frmGenerarConflicto.dispose();
				MainForm.frmInicio.setVisible(true);
			}
		});
		btnCancelar.setFont(new Font("Lucida Sans", Font.BOLD, 15));
		frmGenerarConflicto.getContentPane().add(btnCancelar);
		
		JLabel fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(VentanaNuevoConflicto.class.getResource("/imagenes/Fondo Conflictoo.png")));
		fondo.setBounds(0, 0, 514, 225);
		frmGenerarConflicto.getContentPane().add(fondo);
	}
}
