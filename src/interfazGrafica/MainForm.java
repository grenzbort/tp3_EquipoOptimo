package interfazGrafica;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import negocio.Serializacion;

import javax.swing.ImageIcon;

public class MainForm
{

	static JFrame frmInicio;

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			@SuppressWarnings("static-access")
			public void run()
			{
				try
				{		
					MainForm window = new MainForm();
					window.frmInicio.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm()
	{
		initialize();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize()
	{
		frmInicio = new JFrame();
		frmInicio.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBorrarJugador.class.getResource("/imagenes/Icono Ventana.png")));
		frmInicio.setTitle("Generador de Equipo Optimo ");
		frmInicio.setResizable(false);
		frmInicio.getContentPane().setFont(new Font("Arial", Font.BOLD, 12));
		frmInicio.setFont(new Font("Arial", Font.PLAIN, 12));
		frmInicio.setBounds(100, 100, 598, 471);
		frmInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmInicio.getContentPane().setLayout(null);
		
		JComboBox boxFormacion = new JComboBox();
		boxFormacion.setBounds(401, 361, 126, 41);
		boxFormacion.setModel(new DefaultComboBoxModel(new String[]	{"4-3-3", "4-4-2", "3-4-3", "4-5-1", "5-3-2", "3-5-2", "5-4-1"}));
		boxFormacion.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 12));
		frmInicio.getContentPane().add(boxFormacion);
		
		JButton btnGenerarEquipo = new JButton("GENERAR EQUIPO");
		btnGenerarEquipo.setBounds(10, 343, 326, 88);
		btnGenerarEquipo.setFont(new Font("Lucida Sans", Font.PLAIN, 25));
		btnGenerarEquipo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				VentanaEstiloEquipo.main(boxFormacion.getSelectedItem().toString());
				frmInicio.setVisible(false);
			}
		});
		frmInicio.getContentPane().add(btnGenerarEquipo);
		
		JButton btnAgregarJugador = new JButton("AGREGAR JUGADOR");
		btnAgregarJugador.setBounds(346, 133, 236, 50);
		btnAgregarJugador.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				frmInicio.setVisible(false);
				VentanaNuevoJugador.ingresoJugador();
			}
		});
		btnAgregarJugador.setFont(new Font("Lucida Sans", Font.PLAIN, 17));
		frmInicio.getContentPane().add(btnAgregarJugador);
		
		JButton btnBorrarJugador = new JButton("BORRAR JUGADOR");
		btnBorrarJugador.setBounds(346, 194, 236, 50);
		btnBorrarJugador.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				frmInicio.setVisible(false);
				VentanaBorrarJugador.borrarJugador();
			}
		});
		btnBorrarJugador.setFont(new Font("Lucida Sans", Font.PLAIN, 17));
		frmInicio.getContentPane().add(btnBorrarJugador);
		
		JButton btnAgregarIncompatibilidad = new JButton("CONFLICTOS");
		btnAgregarIncompatibilidad.setBounds(346, 255, 236, 50);
		btnAgregarIncompatibilidad.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				frmInicio.setVisible(false);
				VentanaNuevoConflicto.ingresoIncompatibilidad();
			}
		});
		btnAgregarIncompatibilidad.setFont(new Font("Lucida Sans", Font.PLAIN, 17));
		frmInicio.getContentPane().add(btnAgregarIncompatibilidad);
		
		JLabel lblFormacion = new JLabel("TIPO DE FORMACION :");
		lblFormacion.setHorizontalAlignment(SwingConstants.CENTER);
		lblFormacion.setBounds(346, 326, 236, 32);
		lblFormacion.setFont(new Font("Lucida Sans", Font.PLAIN, 18));
		frmInicio.getContentPane().add(lblFormacion);
		
		JButton btnCargarEquipo = new JButton("CARGAR EQUIPO");
		btnCargarEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				Serializacion.cargarJugadores();
			}
		});
		btnCargarEquipo.setFont(new Font("Lucida Sans", Font.PLAIN, 17));
		btnCargarEquipo.setBounds(346, 72, 236, 50);
		frmInicio.getContentPane().add(btnCargarEquipo);
		
		JButton btnGuardarEquipo = new JButton("GUARDAR EQUIPO");
		btnGuardarEquipo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				Serializacion.guardarJugadores();
			}
		});
		btnGuardarEquipo.setFont(new Font("Lucida Sans", Font.PLAIN, 17));
		btnGuardarEquipo.setBounds(346, 11, 236, 50);
		frmInicio.getContentPane().add(btnGuardarEquipo);
		
		JLabel fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(MainForm.class.getResource("/imagenes/Fondo Inicio.png")));
		fondo.setBounds(0, 0, 592, 442);
		frmInicio.getContentPane().add(fondo);		
	}
}
