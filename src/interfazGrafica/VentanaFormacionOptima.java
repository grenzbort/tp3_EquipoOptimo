package interfazGrafica;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import negocio.Equipo;
import negocio.Jugador;

import java.awt.TextArea;
import javax.swing.border.LineBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import java.awt.Insets;
import javax.swing.DropMode;
import java.awt.Rectangle;
import javax.swing.JPanel;

@SuppressWarnings("unused")
public class VentanaFormacionOptima
{
	public JFrame frmFormacionOptima;
	static Equipo equipo;
	static String formacion;
	public JTextArea datosJugador;
	
	public static void resultado(Equipo equipoIdeal, String estFormacion)
	{
		equipo = equipoIdeal;
		formacion = estFormacion;
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try {
					VentanaFormacionOptima window = new VentanaFormacionOptima();
					window.frmFormacionOptima.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaFormacionOptima() 
	{
		initialize();
	}

	private void initialize()
	{
		frmFormacionOptima = new JFrame();
		frmFormacionOptima.setResizable(false);
		frmFormacionOptima.setTitle("Formacion Optima");
		frmFormacionOptima.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaFormacionOptima.class.getResource("/imagenes/Icono Ventana.png")));
		frmFormacionOptima.setBounds(0, 0, 985, 981);
		frmFormacionOptima.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFormacionOptima.getContentPane().setLayout(null);
		
		boolean DFC1 = false, DFC2 = false, MC1 = false, MC2 = false, DC1 = false;
		
		datosJugador = new JTextArea();
		datosJugador.setForeground(Color.WHITE);
		datosJugador.setEditable(false);
		datosJugador.setOpaque(false);
		datosJugador.setFont(new Font("Lucida Sans", Font.BOLD, 25));
		datosJugador.setBounds(570, 696, 403, 173);
		frmFormacionOptima.getContentPane().add(datosJugador);
		
		JLabel DatosJugador = new JLabel("Datos Jugador");
		DatosJugador.setFont(new Font("Lucida Sans", Font.PLAIN, 26));
		DatosJugador.setForeground(Color.BLACK);
		DatosJugador.setBounds(570, 661, 192, 34);
		frmFormacionOptima.getContentPane().add(DatosJugador);
		
		JLabel puntoArquero = new JLabel("");
		puntoArquero.setBounds(79, 257, 50, 50);
		puntoArquero.setVerticalAlignment(SwingConstants.TOP);
		frmFormacionOptima.getContentPane().add(puntoArquero);
		
		JLabel puntoLD = new JLabel("");
		puntoLD.setBounds(307, 482, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoLD);
		
		JLabel puntoDCD = new JLabel("");
		puntoDCD.setBounds(190, 370, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoDCD);
		
		JLabel puntoDFC = new JLabel("");
		puntoDFC.setBounds(174, 257, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoDFC);
		
		JLabel puntoDCI = new JLabel("");
		puntoDCI.setBounds(190, 152, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoDCI);
		
		JLabel puntoLI = new JLabel("");
		puntoLI.setBounds(307, 40, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoLI);
		
		JLabel puntoMED = new JLabel("");
		puntoMED.setBounds(619, 482, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoMED);
		
		JLabel puntoMCD = new JLabel("");
		puntoMCD.setBounds(490, 370, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoMCD);
		
		JLabel puntoMC = new JLabel("");
		puntoMC.setBounds(377, 257, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoMC);
		
		JLabel puntoMCI = new JLabel("");
		puntoMCI.setBounds(490, 152, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoMCI);
		
		JLabel puntoMEI = new JLabel("");
		puntoMEI.setBounds(619, 40, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoMEI);
		
		JLabel puntoDED = new JLabel("");
		puntoDED.setBounds(775, 400, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoDED);
		
		JLabel puntoDC = new JLabel("");
		puntoDC.setBounds(739, 257, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoDC);
		
		JLabel puntoDEI = new JLabel("");
		puntoDEI.setBounds(775, 121, 50, 50);
		frmFormacionOptima.getContentPane().add(puntoDEI);
		
		JButton botonAceptar = new JButton("ACEPTAR");
		botonAceptar.setBounds(855, 909, 118, 34);
		botonAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainForm.frmInicio.setVisible(true);
				frmFormacionOptima.dispose();
			}
		});
		frmFormacionOptima.getContentPane().add(botonAceptar);
		
		JLabel lblCalidad = new JLabel("CALIDAD TOTAL DEL EQUIPO :   "+ equipo.getCalidad());
		lblCalidad.setForeground(Color.BLACK);
		lblCalidad.setBounds(45, 590, 478, 34);
		lblCalidad.setFont(new Font("Lucida Sans", Font.BOLD, 26));
		frmFormacionOptima.getContentPane().add(lblCalidad);
		
		TextArea txtAreaConflictos = new TextArea();
		txtAreaConflictos.setBounds(45, 680, 280, 223);
		txtAreaConflictos.setEditable(false);
		
		if(VentanaEstiloEquipo.rdbtnSi.isSelected())
		{
			frmFormacionOptima.getContentPane().add(txtAreaConflictos);
			JLabel lblConflictos = new JLabel("CONFLICTOS");
			lblConflictos.setForeground(Color.BLACK);
			lblConflictos.setFont(new Font("Lucida Sans", Font.BOLD, 26));
			lblConflictos.setBounds(45, 635, 290, 34);
			frmFormacionOptima.getContentPane().add(lblConflictos);
		}
		String conflictos = "";

		for(Jugador j: Equipo.getEquipo())
		{	
			if(VentanaEstiloEquipo.rdbtnSi.isSelected())
			{
				if(!j.incompatibles.isEmpty()&& !j.incompatibles.contains(conflictos))
				{
					for(String j2 : j.incompatibles){
						conflictos = conflictos + j.nombreYapellido+" - "+j2+"\n";
					}
				}
			}
			txtAreaConflictos.setText(conflictos);
			
			if(j.posicion.equals("Arquero"))
			{
				puntoArquero.setIcon(cargarImagen("Arquero"));
				clikJugador(puntoArquero, j, "Arquero");
			}
			else if(j.posicion.equals("Defensor"))
			{
				if(j.posicionEspecifica.equals("LD"))
				{
					puntoLD.setIcon(cargarImagen("Defensor"));
					clikJugador(puntoLD, j, "Defensor");
				}
				else if(j.posicionEspecifica.equals("LI"))
				{
					puntoLI.setIcon(cargarImagen("Defensor"));
					clikJugador(puntoLI, j, "Defensor");
				}
				else
				{
					if(!DFC1 && !(equipo.cantDef()==3))
					{
						puntoDCD.setIcon(cargarImagen("Defensor"));
						clikJugador(puntoDCD, j, "Defensor");
						DFC1 = true;
					}
					else if(!DFC2 && !(equipo.cantDef()==3))
					{
						puntoDCI.setIcon(cargarImagen("Defensor"));
						clikJugador(puntoDCI, j, "Defensor");
						DFC2 = true;
					}
					else
					{
						puntoDFC.setIcon(cargarImagen("Defensor"));
						clikJugador(puntoDFC, j, "Defensor");
					}
				}		
			}
			else if(j.posicion.equals("Volante"))
			{
				if(j.posicionEspecifica.equals("MD"))
				{
					puntoMED.setIcon(cargarImagen("Volante"));
					clikJugador(puntoMED, j, "Volante");
				}
				else if(j.posicionEspecifica.equals("MI"))
				{
					puntoMEI.setIcon(cargarImagen("Volante"));
					clikJugador(puntoMEI, j, "Volante");
				}
				else
				{
					if(equipo.cantVol()==3)
					{
						puntoMC.setIcon(cargarImagen("Volante"));
						clikJugador(puntoMC, j, "Volante");
					}
					else if(!MC1)
					{
						puntoMCD.setIcon(cargarImagen("Volante"));
						clikJugador(puntoMCD, j, "Volante");
						MC1=true;
					}
					else if(!MC2)
					{
						puntoMCI.setIcon(cargarImagen("Volante"));
						clikJugador(puntoMCI, j, "Volante");
						MC2=true;
					}
					else
					{
						puntoMC.setIcon(cargarImagen("Volante"));
						clikJugador(puntoMC, j, "Volante");
					}
				}
			}
			else
			{
				if(equipo.cantDel()==3)
				{
					if(j.posicionEspecifica.equals("ED"))
					{
						puntoDED.setIcon(cargarImagen("Delantero"));
						clikJugador(puntoDED, j, "Delantero");
					}
					else if(j.posicionEspecifica.equals("EI"))
					{
						puntoDEI.setIcon(cargarImagen("Delantero"));
						clikJugador(puntoDEI, j, "Delantero");
					}
					else
					{
						puntoDC.setIcon(cargarImagen("Delantero"));
						clikJugador(puntoDC, j, "Delantero");
					}
				}
				else if(equipo.cantDel()==1)
				{
					puntoDC.setIcon(cargarImagen("Delantero"));
					clikJugador(puntoDC, j, "Delantero");
				}
				else
				{
					if(!DC1)
					{
						puntoDED.setIcon(cargarImagen("Delantero"));
						clikJugador(puntoDED, j, "Delantero");
						DC1=true;
					}
					else
					{
						puntoDEI.setIcon(cargarImagen("Delantero"));
						clikJugador(puntoDEI, j, "Delantero");
					}
				}
			}
		}
		
		JLabel fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(VentanaFormacionOptima.class.getResource("/imagenes/Fondo Formacion Optima.png")));
		fondo.setBounds(0, 0, 983, 954);
		frmFormacionOptima.getContentPane().add(fondo);
	}

	private void clikJugador(JLabel punto, Jugador j, String posicion)
	{
		punto.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent arg0)
			{
				if(posicion.equals("Arquero"))
				{
					punto.setIcon(cargarImagen("ArqueroPresionado"));
				}
				else if(posicion.equals("Defensor"))
				{
					punto.setIcon(cargarImagen("DefensorPresionado"));
				}
				else if(posicion.equals("Volante"))
				{
					punto.setIcon(cargarImagen("VolantePresionado"));
				}
				else
				{
					punto.setIcon(cargarImagen("DelanteroPresionado"));
				}
				datosJugador.setText(datos(j));
			}
			
			@Override
			public void mouseReleased(MouseEvent e)
			{
				if(posicion.equals("Arquero"))
				{
					punto.setIcon(cargarImagen("Arquero"));
				}
				else if(posicion.equals("Defensor"))
				{
					punto.setIcon(cargarImagen("Defensor"));
				}
				else if(posicion.equals("Volante"))
				{
					punto.setIcon(cargarImagen("Volante"));
				}
				else
				{
					punto.setIcon(cargarImagen("Delantero"));
				}
				datosJugador.setText("");
			}
		});
	}

	static ImageIcon cargarImagen(String posicionJugador)
	{
		ImageIcon imagen;
		File img = new File("src/imagenes/"+posicionJugador+".png");
		imagen = ResizeImage("src/imagenes/"+posicionJugador+".png");
		return imagen;
	}

	static ImageIcon ResizeImage(String imgPath)
	{
		ImageIcon MyImage = new ImageIcon(imgPath);
		Image img = MyImage.getImage();
		Image newImage = img.getScaledInstance(40, 40,Image.SCALE_SMOOTH);
		ImageIcon image = new ImageIcon(newImage);
		return image;
	}
	
	private String datos(Jugador j)
	{
		return 	"Nombre: "+ j.nombreYapellido+"\n"+
				"Calidad: "+j.calidad+"\n"+
				"Posicion: "+j.posicionEspecifica;
	}
}
