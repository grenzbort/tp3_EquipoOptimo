package interfazGrafica;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JRadioButton;

import negocio.PrepararEquipo;

import java.awt.Color;

public class VentanaEstiloEquipo
{

	static JFrame frmEstiloDelEquipo;
	static String formacion;
	static JRadioButton rdbtnSi;
	
	public static void main(String formacio)
	{
		formacion = formacio;
		EventQueue.invokeLater(new Runnable()
		{
			@SuppressWarnings("static-access")
			public void run()
			{
				try
				{
					VentanaEstiloEquipo window = new VentanaEstiloEquipo();
					window.frmEstiloDelEquipo.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaEstiloEquipo()
	{
		initialize();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize()
	{
		frmEstiloDelEquipo = new JFrame();
		frmEstiloDelEquipo.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBorrarJugador.class.getResource("/imagenes/Icono Ventana.png")));
		frmEstiloDelEquipo.setTitle("Estilo del equipo");
		frmEstiloDelEquipo.setResizable(false);
		frmEstiloDelEquipo.setBounds(100, 100, 430, 267);
		frmEstiloDelEquipo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEstiloDelEquipo.getContentPane().setLayout(null);
		
		JLabel lblSeleccion = new JLabel("Estilo de juego del equipo");
		lblSeleccion.setForeground(Color.BLACK);
		lblSeleccion.setBackground(Color.WHITE);
		lblSeleccion.setFont(new Font("Lucida Sans", Font.BOLD, 14));
		lblSeleccion.setBounds(10, 11, 212, 30);
		frmEstiloDelEquipo.getContentPane().add(lblSeleccion);
		
		JComboBox boxSeleccion = new JComboBox();
		boxSeleccion.setFont(new Font("Lucida Sans", Font.BOLD, 16));
		boxSeleccion.setModel(new DefaultComboBoxModel(new String[] {"Mejor arquero", "Mejor defensa", "Mejor mediocampo", "Mejor delantera"}));
		boxSeleccion.setBounds(10, 52, 202, 30);
		frmEstiloDelEquipo.getContentPane().add(boxSeleccion);
		
	    rdbtnSi = new JRadioButton("SI");
		rdbtnSi.setFont(new Font("Lucida Sans", Font.BOLD, 14));
		rdbtnSi.setBounds(50, 137, 58, 23);
		frmEstiloDelEquipo.getContentPane().add(rdbtnSi);
		
		JRadioButton rdbtnNo = new JRadioButton("NO");
		rdbtnNo.setFont(new Font("Lucida Sans", Font.BOLD, 14));
		rdbtnNo.setBounds(120, 137, 58, 23);
		frmEstiloDelEquipo.getContentPane().add(rdbtnNo);
		
		ButtonGroup grupo = new ButtonGroup();
		grupo.add(rdbtnSi);
		grupo.add(rdbtnNo);
		
		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.addActionListener(new ActionListener()
		{
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent arg0)
			{
				PrepararEquipo preparacion = new PrepararEquipo(formacion, eleccionAlgoritmo(boxSeleccion.getSelectedIndex()), rdbtnSi.isSelected());
				frmEstiloDelEquipo.dispose();
			}
		});
		btnAceptar.setFont(new Font("Lucida Sans", Font.BOLD, 16));
		btnAceptar.setBounds(52, 175, 121, 37);
		frmEstiloDelEquipo.getContentPane().add(btnAceptar);
		
		JLabel lblConIncompatibles = new JLabel("Hay conflictos en el equipo?");
		lblConIncompatibles.setFont(new Font("Lucida Sans", Font.BOLD, 14));
		lblConIncompatibles.setBounds(10, 105, 212, 30);
		frmEstiloDelEquipo.getContentPane().add(lblConIncompatibles);
		
		JLabel fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(VentanaEstiloEquipo.class.getResource("/imagenes/Fondo Estilo Equipo.png")));
		fondo.setBounds(236, 0, 188, 238);
		frmEstiloDelEquipo.getContentPane().add(fondo);
		
	}
	
	private String eleccionAlgoritmo(int indice)
	{
		if(indice==0)
			return "ARQ";
		if(indice==1)
			return "DEF";
		if(indice==2)
			return "VOL";
		return "DEL";
	}
}
