package interfazGrafica;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import negocio.JugadorAJson;
import negocio.Jugador;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class VentanaNuevoJugador
{

	static JFrame frmAgregarJugador;
	private JTextField txtNombreYapellido;
	@SuppressWarnings("rawtypes")
	JComboBox boxPosicionEspecifica;

	public static void ingresoJugador()
	{
		EventQueue.invokeLater(new Runnable()
		{
			@SuppressWarnings("static-access")
			public void run()
			{
				try
				{
					VentanaNuevoJugador window = new VentanaNuevoJugador();
					window.frmAgregarJugador.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaNuevoJugador()
	{
		initialize();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize()
	{
		frmAgregarJugador = new JFrame();
		frmAgregarJugador.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBorrarJugador.class.getResource("/imagenes/Icono Ventana.png")));
		frmAgregarJugador.setTitle("Agregar jugador");
		frmAgregarJugador.setResizable(false);
		frmAgregarJugador.getContentPane().setFont(new Font("Arial", Font.PLAIN, 12));
		frmAgregarJugador.setBounds(100, 100, 512, 312);
		frmAgregarJugador.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgregarJugador.getContentPane().setLayout(null);

		JLabel lblNombreApellido = new JLabel("Nombre y Apellido");
		lblNombreApellido.setBounds(321, 10, 131, 24);
		lblNombreApellido.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		frmAgregarJugador.getContentPane().add(lblNombreApellido);

		JLabel lblCalidad = new JLabel("Calidad");
		lblCalidad.setBounds(321, 60, 131, 24);
		lblCalidad.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		frmAgregarJugador.getContentPane().add(lblCalidad);

		JLabel lblPosicion = new JLabel("Posicion");
		lblPosicion.setBounds(321, 110, 131, 24);
		lblPosicion.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		frmAgregarJugador.getContentPane().add(lblPosicion);

		JComboBox boxCalidad = new JComboBox();
		boxCalidad.setBounds(321, 84, 175, 22);
		boxCalidad.setModel(new DefaultComboBoxModel(new String[]{"1","2","3","4","5","6","7","8","9","10"}));
		boxCalidad.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		boxCalidad.setMaximumRowCount(10);
		frmAgregarJugador.getContentPane().add(boxCalidad);

		JComboBox boxPosicion = new JComboBox();
		boxPosicion.setBounds(321, 136, 175, 22);
		boxPosicion.setModel(new DefaultComboBoxModel(new String[]{"Arquero","Defensor","Volante","Delantero"}));
		boxPosicion.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				completarPosEspecifica((String) boxPosicion.getSelectedItem());
			}
		});
		boxPosicion.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		boxPosicion.setMaximumRowCount(4);
		frmAgregarJugador.getContentPane().add(boxPosicion);

		txtNombreYapellido = new JTextField();
		txtNombreYapellido.setBounds(321, 34, 175, 22);
		txtNombreYapellido.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		txtNombreYapellido.setColumns(10);
		frmAgregarJugador.getContentPane().add(txtNombreYapellido);

		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.setBounds(195, 228, 145, 44);
		btnAceptar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(controlString(txtNombreYapellido.getText()))
				{
					frmAgregarJugador.dispose();
					JugadorAJson.guardarJugador(new Jugador(Integer.parseInt(boxCalidad.getSelectedItem().toString()), 
							boxPosicion.getSelectedItem().toString(), boxPosicionEspecifica.getSelectedItem().toString(),
							txtNombreYapellido.getText()));
				}
			}
		});
		btnAceptar.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		frmAgregarJugador.getContentPane().add(btnAceptar);

		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.setBounds(350, 228, 145, 44);
		btnCancelar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				frmAgregarJugador.dispose();
				MainForm.frmInicio.setVisible(true);
			}
		});
		btnCancelar.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		frmAgregarJugador.getContentPane().add(btnCancelar);

		JLabel lblPosEspecifica = new JLabel("Posicion especifica");
		lblPosEspecifica.setBounds(321, 160, 131, 24);
		lblPosEspecifica.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		frmAgregarJugador.getContentPane().add(lblPosEspecifica);

		boxPosicionEspecifica = new JComboBox();
		boxPosicionEspecifica.setBounds(321, 184, 175, 22);
		boxPosicionEspecifica.setMaximumRowCount(4);
		boxPosicionEspecifica.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		frmAgregarJugador.getContentPane().add(boxPosicionEspecifica);

		JLabel fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(VentanaNuevoJugador.class.getResource("/imagenes/Fondo Agregar Jugador.png")));
		fondo.setBounds(0, 0, 506, 283);
		frmAgregarJugador.getContentPane().add(fondo);
		completarPosEspecifica((String) boxPosicion.getSelectedItem());
	}

	@SuppressWarnings("unchecked")
	void completarPosEspecifica(String posicion){
		boxPosicionEspecifica.removeAllItems();
		if(posicion.equals("Defensor"))
		{
			boxPosicionEspecifica.addItem("LD");
			boxPosicionEspecifica.addItem("LI");
			boxPosicionEspecifica.addItem("DFC");
		}
		else if(posicion.equals("Volante"))
		{
			boxPosicionEspecifica.addItem("MD");
			boxPosicionEspecifica.addItem("MI");
			boxPosicionEspecifica.addItem("MC");
		}
		else if(posicion.equals("Delantero"))
		{
			boxPosicionEspecifica.addItem("ED");
			boxPosicionEspecifica.addItem("EI");
			boxPosicionEspecifica.addItem("DC");
		}
		else{
			boxPosicionEspecifica.addItem("Arquero");
		}
	}

	private boolean controlString(String palabra)
	{	
		if(!palabra.matches("[a-zA-z_\\s]*") || palabra.isEmpty()){
			frmAgregarJugador.setVisible(false);
			Notificacion.notificar("El nombre y el apellido solo debe contener letras.", "ingresoJugador");
			return false;
		}
		return true;
	}	
}
