package interfazGrafica;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import negocio.JugadorAJson;
import negocio.Jugador;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class VentanaBorrarJugador
{

	private JFrame frmEliminarJugador;

	public static void borrarJugador()
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					VentanaBorrarJugador window = new VentanaBorrarJugador();
					window.frmEliminarJugador.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaBorrarJugador()
	{
		initialize();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize()
	{
		frmEliminarJugador = new JFrame();
		frmEliminarJugador.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBorrarJugador.class.getResource("/imagenes/Icono Ventana.png")));
		frmEliminarJugador.setTitle("Eliminar jugador");
		frmEliminarJugador.setResizable(false);
		frmEliminarJugador.setBounds(100, 100, 381, 289);
		frmEliminarJugador.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEliminarJugador.getContentPane().setLayout(null);

		JComboBox boxJugadores = new JComboBox();
		for(Jugador j : JugadorAJson.jugadores)
		{
			boxJugadores.addItem(j.nombreYapellido);
		}
		boxJugadores.setMaximumRowCount(999);
		boxJugadores.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		boxJugadores.setBounds(169, 44, 196, 22);
		frmEliminarJugador.getContentPane().add(boxJugadores);

		JLabel lblSeleccion = new JLabel("Jugador a borrar");
		lblSeleccion.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		lblSeleccion.setBounds(209, 11, 116, 22);
		frmEliminarJugador.getContentPane().add(lblSeleccion);

		JButton btnBorrar = new JButton("BORRAR");
		btnBorrar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				frmEliminarJugador.dispose();
				if(JugadorAJson.jugadores.isEmpty()){
					Notificacion.notificar("No hay ningun jugador para borrar", "Inicio");
				}
				else
				{
					JugadorAJson.borrarJugador(boxJugadores.getSelectedItem().toString());
				}					
			}
		});
		btnBorrar.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		btnBorrar.setBounds(255, 160, 110, 39);
		frmEliminarJugador.getContentPane().add(btnBorrar);
		
		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				frmEliminarJugador.dispose();
				MainForm.frmInicio.setVisible(true);
			}
		});
		btnCancelar.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		btnCancelar.setBounds(255, 210, 110, 39);
		frmEliminarJugador.getContentPane().add(btnCancelar);
		
		JLabel fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(VentanaBorrarJugador.class.getResource("/imagenes/Fondo Borrar jugadoor.png")));
		fondo.setBounds(0, 0, 375, 260);
		frmEliminarJugador.getContentPane().add(fondo);
	}
}
