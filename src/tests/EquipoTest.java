package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import negocio.JugadorAJson;
import negocio.PrepararEquipo;

public class EquipoTest
{
	@Test
	public void equipoVacio()
	{	
		JugadorAJson.setPath("testVacio");
		
		PrepararEquipo codigo = new PrepararEquipo("4-3-3", "DEL", false);
		
		assertEquals(null, codigo.getEquipoIdeal());
	}

	@Test
	public void equipoIncompleto()
	{			
		JugadorAJson.setPath("testFaltanJugadores");
		
		PrepararEquipo codigo = new PrepararEquipo("5-3-2", "DEL", false);
		
		assertEquals(null, codigo.getEquipoIdeal());
	}

	@Test
	public void equipoIncompletoPorLasIncompatibilidades()
	{
		JugadorAJson.setPath("testFaltanPorIncompatibles");
		
		PrepararEquipo codigo = new PrepararEquipo("4-3-3", "DEL", true);
		
		assertEquals(null, codigo.getEquipoIdeal());
	}

	@Test
	public void equipoCompleto()
	{
		JugadorAJson.setPath("testOk");
		
		PrepararEquipo c = new PrepararEquipo("4-3-3", "ARQ", true);
		
		
		assertTrue(c.getEquipoIdeal().equipoCompleto());
		assertEquals(c.getEquipoIdeal().getCalidad(), 84);
		
		PrepararEquipo c2 = new PrepararEquipo("5-4-1", "DEL", true);
		assertEquals(c2.getEquipoIdeal().getCalidad(), 77);
	}
}

