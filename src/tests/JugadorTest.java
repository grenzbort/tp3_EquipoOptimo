package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import negocio.Jugador;

public class JugadorTest
{
	private ArrayList<Jugador> inicializarJugadores()
	{
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		
		Jugador j1 = new Jugador(3, "Defensor", "LI", "Programacion 1");
		Jugador j2 = new Jugador(6, "Volante", "MD", "Programacion 2");
		Jugador j3 = new Jugador(10, "Delantero", "DC", "Programacion 3");

		jugadores.add(j1);
		jugadores.add(j2);
		jugadores.add(j3);
		
		return jugadores;
	}
	@Test
	public void jugadoresIgualesTest()
	{
		Jugador j1 = new Jugador(6, "Defensor", "LI", "Programacion 1");
		Jugador j2 = new Jugador(6, "Defensor", "LI", "Programacion 1");
		
		assertTrue(j1.equals(j2));
	}
	
	@Test
	public void agregarIncompatibilidadTest()
	{
		ArrayList<Jugador> jugadores = inicializarJugadores();
		
		jugadores.get(0).agregarIncompatible(jugadores.get(1));
		
		assertEquals(jugadores.get(0).incompatibles.get(0), jugadores.get(1).nombreYapellido);
		assertEquals(jugadores.get(1).incompatibles.get(0), jugadores.get(0).nombreYapellido);
	}

	@Test
	public void borrarIncompatibilidadTest()
	{	
		ArrayList<Jugador> jugadores = inicializarJugadores();
		
		jugadores.get(0).agregarIncompatible(jugadores.get(2));
		jugadores.get(2).borrarIncompatible(jugadores.get(0));
		
		assertEquals(0, jugadores.get(2).incompatibles.size());
		assertEquals(0, jugadores.get(0).incompatibles.size());
	}
	
	@Test
	public void tieneConflictosConTest()
	{
		ArrayList<Jugador> jugadores = inicializarJugadores();
		
		jugadores.get(0).agregarIncompatible(jugadores.get(2));
	
		assertTrue(jugadores.get(2).tieneConflictosCon(jugadores.get(0)));
	}	
}
